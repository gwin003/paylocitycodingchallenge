﻿namespace PaylocityChallenge.Contracts
{
    public enum DependentTypeEnum
    {
        Spouse = 1,
        Child = 2
    }
}
