﻿
namespace PaylocityChallenge.Contracts.Common
{
    public class PersonContract
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
