﻿namespace PaylocityChallenge.Contracts.Common
{
    public class DependentContract : PersonContract
    {
        public DependentTypeEnum DependentType { get; set; }
    }
}
