﻿namespace PaylocityChallenge.Contracts.Common
{
    public class EmployeeContract : PersonContract
    {
        //employee fields could go here
        public string EmployeeNumber { get; set; }
    }
}
