﻿using System.Collections.Generic;
using PaylocityChallenge.Contracts.Common;

namespace PaylocityChallenge.Contracts.BenefitEstimate
{
    public class EstimateRequestContract
    {
        public EstimateRequestContract()
        {
            Dependents = new List<DependentContract>();
        }

        public EmployeeContract Employee { get; set; }

        public List<DependentContract> Dependents { get; set; }
    }
}
