﻿ namespace PaylocityChallenge.Contracts.BenefitEstimate
{
    public class EstimateResultContract
    {
        public decimal PerPaycheckCost { get; set; }
        public decimal YearlyCost { get; set; }
        public decimal NetPayAmount { get; set; }
    }
}
