﻿using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;

namespace PaylocityChallenge.Core.BenefitCalculator
{
    public class GetEmployeePaycheckAmount : IGetEmployeePaycheckAmount
    {
        private readonly IEmployeePayRepository _employeePayRepository;

        public GetEmployeePaycheckAmount(IEmployeePayRepository employeePayRepository)
        {
            _employeePayRepository = employeePayRepository;
        }

        public decimal GetAmount()
        {
            var paychecksPerYear = _employeePayRepository.PaychecksPerYear();
            if (paychecksPerYear == 0) return 0;

            var grossPaycheckAmount = _employeePayRepository.GetYearlySalary() / paychecksPerYear;

            return grossPaycheckAmount;
        }
    }
}
