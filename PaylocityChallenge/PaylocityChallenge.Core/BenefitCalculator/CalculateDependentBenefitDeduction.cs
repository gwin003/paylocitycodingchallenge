﻿using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;

namespace PaylocityChallenge.Core.BenefitCalculator
{
    public class CalculateDependentBenefitDeduction : AbstractCalculateBenefitDeduction<DependentContract>, ICalculateDependentBenefitDeduction
    {
        public CalculateDependentBenefitDeduction(ICalculateBenefitDiscount calculateBenefitDiscount, IDependentBenefitCostRepository costRepository) 
            : base(calculateBenefitDiscount, costRepository)
        {
        }
    }
}
