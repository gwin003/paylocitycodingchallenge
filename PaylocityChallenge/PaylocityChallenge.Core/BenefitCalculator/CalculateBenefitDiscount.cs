﻿using System.Collections.Generic;
using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;

namespace PaylocityChallenge.Core.BenefitCalculator
{
    public class CalculateBenefitDiscount : ICalculateBenefitDiscount
    {
        private Dictionary<char, decimal> _discountsByName = new Dictionary<char, decimal>
        {
            { 'a', 0.1m }
        };

        public decimal CalculatePercentDiscount(PersonContract person)
        {
            var name = person.FirstName?.ToLower();

            if (string.IsNullOrEmpty(name))
            {
                return 1;
            }

            _discountsByName.TryGetValue(name[0], out var percentDiscount);
            return 1 - percentDiscount;
        }
    }
}
