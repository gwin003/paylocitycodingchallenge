﻿using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;

namespace PaylocityChallenge.Core.BenefitCalculator
{
    public class CalculateEmployeeBenefitDeduction : AbstractCalculateBenefitDeduction<EmployeeContract>, ICalculateEmployeeBenefitDeduction
    {
        public CalculateEmployeeBenefitDeduction(ICalculateBenefitDiscount calculateBenefitDiscount, IEmployeeBenefitCostRepository costRepository) 
            : base(calculateBenefitDiscount, costRepository)
        {
        }
    }
}
