﻿using PaylocityChallenge.Contracts.Common;

namespace PaylocityChallenge.Core.BenefitCalculator.Interfaces
{
    public interface ICalculateBenefitDiscount
    {
        decimal CalculatePercentDiscount(PersonContract person);
    }
}
