﻿using PaylocityChallenge.Contracts.Common;

namespace PaylocityChallenge.Core.BenefitCalculator.Interfaces
{
    public interface ICalculateBenefitDeduction<T> where T : class
    {
        decimal CalculateYearlyDeduction(T contract);
    }    

    public interface ICalculateEmployeeBenefitDeduction : ICalculateBenefitDeduction<EmployeeContract> { }

    public interface ICalculateDependentBenefitDeduction : ICalculateBenefitDeduction<DependentContract> { }
}
