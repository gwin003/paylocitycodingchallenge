﻿namespace PaylocityChallenge.Core.BenefitCalculator.Interfaces
{
    public interface IGetEmployeePaycheckAmount
    {
        decimal GetAmount();
    }
}
