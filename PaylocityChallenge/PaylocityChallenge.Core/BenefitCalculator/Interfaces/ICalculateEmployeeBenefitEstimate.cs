﻿using PaylocityChallenge.Contracts;
using PaylocityChallenge.Contracts.BenefitEstimate;

namespace PaylocityChallenge.Core.BenefitCalculator.Interfaces
{
    public interface ICalculateEmployeeBenefitEstimate
    {
        EstimateResultContract Execute(EstimateRequestContract contract);
    }
}
