﻿using PaylocityChallenge.Contracts.BenefitEstimate;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;

namespace PaylocityChallenge.Core.BenefitCalculator
{
    public class CalculateEmployeeBenefitEstimate : ICalculateEmployeeBenefitEstimate
    {
        private readonly ICalculateEmployeeBenefitDeduction _calculateEmployeeBenefit;
        private readonly ICalculateDependentBenefitDeduction _calculateDependentBenefit;
        private readonly IGetEmployeePaycheckAmount _getEmployeePaycheckGross;
        private readonly IEmployeePayRepository _employeePayRepository;
        
        public CalculateEmployeeBenefitEstimate(ICalculateEmployeeBenefitDeduction calculateEmployeeBenefit,
            ICalculateDependentBenefitDeduction calculateDependentBenefit, IGetEmployeePaycheckAmount getEmployeePaycheckGross, 
            IEmployeePayRepository employeePayRepository)
        {
            _calculateEmployeeBenefit = calculateEmployeeBenefit;
            _calculateDependentBenefit = calculateDependentBenefit;
            _getEmployeePaycheckGross = getEmployeePaycheckGross;
            _employeePayRepository = employeePayRepository;
        }

        public EstimateResultContract Execute(EstimateRequestContract contract)
        {
            //get number of paychecks per year
            var paychecksPerYear = _employeePayRepository.PaychecksPerYear();

            //get gross amount of pay for employee
            var grossPaycheckAmount = _getEmployeePaycheckGross.GetAmount();

            //calculate employee deductions
            var perYearCost = _calculateEmployeeBenefit.CalculateYearlyDeduction(contract.Employee);
            
            //calculate dependent deductions
            contract.Dependents.ForEach(x => perYearCost += _calculateDependentBenefit.CalculateYearlyDeduction(x));

            var perPaycheckCost = perYearCost / paychecksPerYear;
            return new EstimateResultContract
            {
                PerPaycheckCost = perPaycheckCost,
                YearlyCost = perYearCost,
                NetPayAmount = grossPaycheckAmount - perPaycheckCost
            };
        }
    }
}
