﻿using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;

namespace PaylocityChallenge.Core.BenefitCalculator
{
    public abstract class AbstractCalculateBenefitDeduction<T> : ICalculateBenefitDeduction<T> where T : PersonContract
    {
        private readonly ICalculateBenefitDiscount _calculateBenefitDiscount;
        private readonly IBenefitCostRepository _benefitCostRepository;

        protected AbstractCalculateBenefitDeduction(ICalculateBenefitDiscount calculateBenefitDiscount, IBenefitCostRepository benefitCostRepository)
        {
            _calculateBenefitDiscount = calculateBenefitDiscount;
            _benefitCostRepository = benefitCostRepository;
        }

        public decimal CalculateYearlyDeduction(T contract)
        {
            return _benefitCostRepository.GetYearlyBenefitCost() * _calculateBenefitDiscount.CalculatePercentDiscount(contract);
        }
    }
}
