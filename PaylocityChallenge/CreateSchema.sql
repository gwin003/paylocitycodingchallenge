﻿

CREATE TABLE dbo.Person (
	Id UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_Person PRIMARY KEY CLUSTERED,
	FirstName VARCHAR (25) NOT NULL,
	LastName VARCHAR (25) NOT NULL,
	DateOfBirth DATE NULL
)

CREATE NONCLUSTERED INDEX IX_Person_Id ON dbo.Person (Id) INCLUDE (FirstName, LastName);

CREATE TABLE dbo.Employee (
	Id UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_Employee PRIMARY KEY CLUSTERED,
	PersonId UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT FK_Employee_Person FOREIGN KEY REFERENCES dbo.Person(Id),
	HireDate DATE NOT NULL
		CONSTRAINT DF_Employee_HireDate DEFAULT GETUTCDATE()
)

CREATE NONCLUSTERED INDEX IX_Employee_PersonId ON dbo.Employee (PersonId);

CREATE TABLE dbo.[Dependent] (
	Id UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_Dependent PRIMARY KEY NONCLUSTERED,
	PersonId UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT FK_Dependent_Person FOREIGN KEY REFERENCES dbo.Person(Id),
	EmployeeId UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT FK_Dependent_Employee FOREIGN KEY REFERENCES dbo.Employee(Id)
)

CREATE CLUSTERED INDEX CIX_Dependent_Employee_Id ON dbo.[Dependent] (EmployeeId, Id)

CREATE TABLE dbo.EmployeeSalary (
	Id UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_EmployeeSalary PRIMARY KEY NONCLUSTERED,
	EmployeeId UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT FK_EmployeeSalary_Employee FOREIGN KEY REFERENCES dbo.Employee(Id),
	YearlySalary DECIMAL(19,4) NOT NULL
)

CREATE CLUSTERED INDEX CIX_EmployeeSalary_Employee_Id ON dbo.EmployeeSalary (EmployeeId, Id)

