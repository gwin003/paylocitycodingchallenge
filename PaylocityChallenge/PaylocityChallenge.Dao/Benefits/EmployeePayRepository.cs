﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaylocityChallenge.Dao.Benefits
{
    /// <summary>
    /// Again, ideally this would hit a real DB. Hardcoding for the sake of time.
    /// </summary>
    public interface IEmployeePayRepository
    {
        int PaychecksPerYear();
        decimal GetYearlySalary();
    }
    public class EmployeePayRepository : IEmployeePayRepository
    {
        public int PaychecksPerYear()
        {
            return 26;
        }

        public decimal GetYearlySalary()
        {
            return 52000m;
        }
    }
}
