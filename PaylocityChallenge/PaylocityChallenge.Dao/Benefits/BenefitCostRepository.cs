﻿namespace PaylocityChallenge.Dao.Benefits
{
    public interface IBenefitCostRepository
    {
        /// <summary>
        /// Ideally, the implementations would call out to a database to get these values. But for the sake of time, I will hardcode them.
        /// </summary>
        /// <returns></returns>
        decimal GetYearlyBenefitCost();
    }

    public interface IDependentBenefitCostRepository : IBenefitCostRepository { }
    public class DependentBenefitCostRepository: IDependentBenefitCostRepository
    {
        public decimal GetYearlyBenefitCost()
        {
            return 500m;
        }
    }

    public interface IEmployeeBenefitCostRepository : IBenefitCostRepository { }
    public class EmployeeBenefitCostRepository : IEmployeeBenefitCostRepository
    {
        public decimal GetYearlyBenefitCost()
        {
            return 1000m;
        }
    }
}
