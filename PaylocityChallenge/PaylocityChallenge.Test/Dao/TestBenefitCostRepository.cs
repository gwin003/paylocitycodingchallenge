﻿using PaylocityChallenge.Dao.Benefits;
using Xunit;

namespace PaylocityChallenge.Test.Dao
{
    /// <summary>
    /// Since the implementation is "mocked" to not hit the database, these tests are not the most useful. 
    /// </summary>
    public class TestBenefitCostRepository
    {
        private readonly IBenefitCostRepository _dependentBenefitCostRepository;
        private readonly IBenefitCostRepository _employeeBenefitCostRepository;

        public TestBenefitCostRepository()
        {
            _dependentBenefitCostRepository = new DependentBenefitCostRepository();
            _employeeBenefitCostRepository = new EmployeeBenefitCostRepository();
        }

        [Fact]
        public void ShouldSuccessfullyQueryTheDatabaseForEmployeeCost()
        {
            var result = _employeeBenefitCostRepository.GetYearlyBenefitCost();
            Assert.NotNull(result);
        }

        [Fact]
        public void ShouldSuccessfullyQueryTheDatabaseForDependentCost()
        {
            var result = _dependentBenefitCostRepository.GetYearlyBenefitCost();
            Assert.NotNull(result);
        }
    }
}
