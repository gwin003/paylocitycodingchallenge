﻿using System.Collections.Generic;
using Moq;
using PaylocityChallenge.Contracts;
using PaylocityChallenge.Contracts.BenefitEstimate;
using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;
using Xunit;

namespace PaylocityChallenge.Test.Core
{
    public class TestCalculateEmployeeBenefitEstimate
    {
        private int _equalsPrecision = 2;
        private CalculateEmployeeBenefitEstimate _command;

        private readonly Mock<ICalculateEmployeeBenefitDeduction> _mockCalculateEmployeeBenefit;
        private readonly Mock<ICalculateDependentBenefitDeduction> _mockCalculateDependentBenefit;
        private readonly Mock<IGetEmployeePaycheckAmount> _mockGetEmployeePaycheckAmount;
        private readonly Mock<IEmployeePayRepository> _mockEmployeePayRepo;

        public TestCalculateEmployeeBenefitEstimate()
        {
            _mockCalculateEmployeeBenefit = new Mock<ICalculateEmployeeBenefitDeduction>();
            _mockCalculateDependentBenefit = new Mock<ICalculateDependentBenefitDeduction>();
            _mockGetEmployeePaycheckAmount = new Mock<IGetEmployeePaycheckAmount>();
            _mockEmployeePayRepo = new Mock<IEmployeePayRepository>();

            _mockGetEmployeePaycheckAmount.Setup(x => x.GetAmount()).Returns(2000m);
            _mockEmployeePayRepo.Setup(x => x.PaychecksPerYear()).Returns(26);

            _command = new CalculateEmployeeBenefitEstimate(_mockCalculateEmployeeBenefit.Object, _mockCalculateDependentBenefit.Object, 
                _mockGetEmployeePaycheckAmount.Object, _mockEmployeePayRepo.Object);
        }

        [Fact]
        public void SimpleTest()
        {
            _mockCalculateEmployeeBenefit.Setup(x => x.CalculateYearlyDeduction(It.IsAny<EmployeeContract>()))
                .Returns(1000);
            _mockCalculateDependentBenefit.Setup(x => x.CalculateYearlyDeduction(It.IsAny<DependentContract>()))
                .Returns(0);

            var contract = new EstimateRequestContract
            {
                Employee = new EmployeeContract
                {
                    FirstName = "Erlich",
                    LastName = "Bachman"
                }
            };

            var result = _command.Execute(contract);
            Assert.Equal(38.46m, result.PerPaycheckCost, _equalsPrecision);
        }

        [Fact]
        public void TestWithDiscount()
        {
            _mockCalculateEmployeeBenefit.Setup(x => x.CalculateYearlyDeduction(It.IsAny<EmployeeContract>()))
                .Returns(900);
            _mockCalculateDependentBenefit.Setup(x => x.CalculateYearlyDeduction(It.IsAny<DependentContract>()))
                .Returns(0);

            var contract = new EstimateRequestContract
            {
                Employee = new EmployeeContract
                {
                    FirstName = "Tim",
                    LastName = "Allen"
                }
            };

            var result = _command.Execute(contract);
            Assert.Equal(34.62m, result.PerPaycheckCost, _equalsPrecision);
        }

        [Fact]
        public void TestWithDependents()
        {
            _mockCalculateEmployeeBenefit.Setup(x => x.CalculateYearlyDeduction(It.IsAny<EmployeeContract>()))
                .Returns(1000);
            _mockCalculateDependentBenefit.Setup(x => x.CalculateYearlyDeduction(It.IsAny<DependentContract>()))
                .Returns(500);

            var contract = new EstimateRequestContract
            {
                Employee = new EmployeeContract
                {
                    FirstName = "Richard",
                    LastName = "Mario Lemieux"
                },
                Dependents = new List<DependentContract>
                {
                    new DependentContract
                    {
                        DependentType = DependentTypeEnum.Child,
                        FirstName = "Sidney",
                        LastName = "Crosby"
                    }
                }
            };

            var result = _command.Execute(contract);
            Assert.Equal(57.69m, result.PerPaycheckCost, _equalsPrecision);
        }
    }
}
