﻿using Moq;
using PaylocityChallenge.Core.BenefitCalculator;
using PaylocityChallenge.Dao.Benefits;
using Xunit;

namespace PaylocityChallenge.Test.Core.BenefitCalculator
{
    public class TestGetEmployeePaycheckAmount
    {
        private GetEmployeePaycheckAmount _command;
        private Mock<IEmployeePayRepository> _mockEmployeePayRepo;

        public TestGetEmployeePaycheckAmount()
        {
            _mockEmployeePayRepo = new Mock<IEmployeePayRepository>();            
            _command = new GetEmployeePaycheckAmount(_mockEmployeePayRepo.Object);
        }


        [Theory]
        [InlineData(2000, 26, 52000)]
        [InlineData(1, 1, 1)]
        [InlineData(0, 0, 100000)]
        [InlineData(50000, 2, 100000)]
        [InlineData(.1d, 100, 10)]
        public void TestGetAmount(decimal expected, int paysPerYear, decimal yearlySalary)
        {
            _mockEmployeePayRepo.Setup(x => x.PaychecksPerYear()).Returns(paysPerYear);
            _mockEmployeePayRepo.Setup(x => x.GetYearlySalary()).Returns(yearlySalary);

            var result = _command.GetAmount();
            Assert.Equal(expected, result);
        }

    }
}
