﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using PaylocityChallenge.Contracts;
using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;
using Xunit;

namespace PaylocityChallenge.Test.Core.BenefitCalculator
{
    public class TestCalculateEmployeeBenefitDeduction
    {
        private readonly CalculateEmployeeBenefitDeduction _calculateEmployeeBenefitDeduction;
        private readonly Mock<ICalculateBenefitDiscount> _mockCalculateBenefitDiscount;

        public TestCalculateEmployeeBenefitDeduction()
        {
            _mockCalculateBenefitDiscount = new Mock<ICalculateBenefitDiscount>();
            var mockDependentCostRepo = new Mock<IEmployeeBenefitCostRepository>();

            mockDependentCostRepo.Setup(x => x.GetYearlyBenefitCost()).Returns(1000m);

            _calculateEmployeeBenefitDeduction = new CalculateEmployeeBenefitDeduction(_mockCalculateBenefitDiscount.Object, mockDependentCostRepo.Object);
        }

        [Theory]
        [InlineData(1000d, 1d)]
        [InlineData(900d, 0.9d)]
        [InlineData(0d, 0d)]
        [InlineData(2000d, 2d)]
        public void TestCalculation(decimal expectedAmount, decimal discount)
        {
            _mockCalculateBenefitDiscount.Setup(x => x.CalculatePercentDiscount(It.IsAny<PersonContract>()))
                .Returns(discount);
            var expected = _calculateEmployeeBenefitDeduction.CalculateYearlyDeduction(new EmployeeContract());
            Assert.Equal(expectedAmount, expected);
        }
    }
}
