﻿using Moq;
using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;
using Xunit;

namespace PaylocityChallenge.Test.Core.BenefitCalculator
{
    public class TestCalculateDependentBenefitDeduction
    {
        private readonly CalculateDependentBenefitDeduction _calculateDependentBenefitDeduction;
        private readonly Mock<ICalculateBenefitDiscount> _mockCalculateBenefitDiscount;

        public TestCalculateDependentBenefitDeduction()
        {
            _mockCalculateBenefitDiscount = new Mock<ICalculateBenefitDiscount>();
            var mockDependentCostRepo = new Mock<IDependentBenefitCostRepository>();

            mockDependentCostRepo.Setup(x => x.GetYearlyBenefitCost()).Returns(500m);

            _calculateDependentBenefitDeduction = new CalculateDependentBenefitDeduction(_mockCalculateBenefitDiscount.Object, mockDependentCostRepo.Object);
        }

        [Theory]
        [InlineData(500d, 1d)]
        [InlineData(450d, 0.9d)]
        [InlineData(0d, 0d)]
        [InlineData(1000d, 2d)]
        public void TestCalculation(decimal expectedAmount, decimal discount)
        {
            _mockCalculateBenefitDiscount.Setup(x => x.CalculatePercentDiscount(It.IsAny<PersonContract>()))
                .Returns(discount);
            var expected = _calculateDependentBenefitDeduction.CalculateYearlyDeduction(new DependentContract());
            Assert.Equal(expectedAmount, expected);
        }

    }
}
