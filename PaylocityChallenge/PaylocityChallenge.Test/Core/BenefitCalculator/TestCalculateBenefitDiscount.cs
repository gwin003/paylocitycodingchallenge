﻿using System;
using PaylocityChallenge.Contracts;
using PaylocityChallenge.Contracts.Common;
using PaylocityChallenge.Core.BenefitCalculator;
using Xunit;

namespace PaylocityChallenge.Test.Core.BenefitCalculator
{
    public class TestCalculateBenefitDiscount
    {
        private CalculateBenefitDiscount _calculateBenefitDiscount;

        public TestCalculateBenefitDiscount()
        {
            _calculateBenefitDiscount = new CalculateBenefitDiscount();
        }

        [Theory]
        [InlineData(1, "Homer", "Simpson")]
        [InlineData(1, "elon", "musk")]
        [InlineData(0.9, "Aaron", "Rodgers")]
        [InlineData(1, "John", "Adams")]
        [InlineData(0.9, "adam", "Adams")]
        [InlineData(1, "", "")]
        [InlineData(1, null, null)]
        public void ShouldCorrectlyReturnDiscount(decimal expected, string firstName, string lastName)
        {
            var response = _calculateBenefitDiscount.CalculatePercentDiscount(new PersonContract
            {
                FirstName = firstName,
                LastName = lastName
            });

            Assert.Equal(expected, response);
        }

        [Fact]
        public void DemonstrateLiskovPrinciple()
        {
            var employee = new EmployeeContract();
            var dependent = new DependentContract();
            var person = new PersonContract();

            _calculateBenefitDiscount.CalculatePercentDiscount(employee);
            _calculateBenefitDiscount.CalculatePercentDiscount(dependent);
            _calculateBenefitDiscount.CalculatePercentDiscount(person);
        }
    }
}
