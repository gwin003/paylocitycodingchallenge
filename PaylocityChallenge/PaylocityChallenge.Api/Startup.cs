﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using PaylocityChallenge.Core;
using PaylocityChallenge.Core.BenefitCalculator;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;
using PaylocityChallenge.Dao.Benefits;

namespace PaylocityChallenge.Api
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //CORS is the worst
            services.AddCors(x =>
            {
                x.AddPolicy("Cors", policy =>
                    {
                        policy.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials();
                    });
            });

            SetupDependencyInjection(services);
        }

        private void SetupDependencyInjection(IServiceCollection services)
        {
            services.AddSingleton<ICalculateEmployeeBenefitEstimate, CalculateEmployeeBenefitEstimate>();
            services.AddSingleton<ICalculateBenefitDiscount, CalculateBenefitDiscount>();
            services.AddSingleton<ICalculateEmployeeBenefitDeduction, CalculateEmployeeBenefitDeduction>();
            services.AddSingleton<ICalculateDependentBenefitDeduction, CalculateDependentBenefitDeduction>();

            services.AddSingleton<IEmployeeBenefitCostRepository, EmployeeBenefitCostRepository>();
            services.AddSingleton<IDependentBenefitCostRepository, DependentBenefitCostRepository>();
            services.AddSingleton<IEmployeePayRepository, EmployeePayRepository>();
            services.AddSingleton<IGetEmployeePaycheckAmount, GetEmployeePaycheckAmount>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("Cors");
            app.UseMvc();
        }
    }
}
