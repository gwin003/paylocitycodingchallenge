﻿using Microsoft.AspNetCore.Mvc;
using PaylocityChallenge.Contracts;
using PaylocityChallenge.Contracts.BenefitEstimate;
using PaylocityChallenge.Core.BenefitCalculator.Interfaces;

namespace PaylocityChallenge.Api.Benefits
{
    [ApiController]
    [Route("api/[controller]")]
    public class BenefitsController : Controller
    {
        private readonly ICalculateEmployeeBenefitEstimate _calculateEmployeeBenefitEstimate;

        public BenefitsController(ICalculateEmployeeBenefitEstimate calculateEmployeeBenefitEstimate)
        {
            _calculateEmployeeBenefitEstimate = calculateEmployeeBenefitEstimate;
        }

        [HttpPost]
        public EstimateResultContract EstimatePerPaycheckBenefitCost(EstimateRequestContract contract)
        {
            return _calculateEmployeeBenefitEstimate.Execute(contract);
        }
    }
}
