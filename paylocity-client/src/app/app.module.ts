import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BenefitEstimatorComponent } from './benefit-estimator/benefit-estimator.component';
import { BenefitEstimatorService } from './benefit-estimator/services/benefit-estimator.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { MatInputModule } from '@angular/material/input';
import { BenefitEstimatorResultsComponent } from './benefit-estimator/benefit-estimator-results/benefit-estimator-results.component';
import { BenefitEstimatorRequestComponent } from './benefit-estimator/benefit-estimator-request/benefit-estimator-request.component';


@NgModule({
  declarations: [
    AppComponent,
    BenefitEstimatorComponent,
    BenefitEstimatorResultsComponent,
    BenefitEstimatorRequestComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    MatInputModule
  ],
  providers: [
    BenefitEstimatorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
