import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BenefitEstimateRequestModel } from '../models/benefit-estimate-request';
import { Observable } from 'rxjs';
import { BenefitEstimateResponseModel } from '../models/benefit-estimate-response';
import { environment } from 'src/environments/environment';

@Injectable()
export class BenefitEstimatorService {

    constructor(private httpClient: HttpClient) {}

    postBenefitEstimate(request: BenefitEstimateRequestModel): Observable<BenefitEstimateResponseModel> {
        return this.httpClient.post<BenefitEstimateResponseModel>(environment.urls.benefitEstimator.postBenefitEstimate, request);
    }
}
