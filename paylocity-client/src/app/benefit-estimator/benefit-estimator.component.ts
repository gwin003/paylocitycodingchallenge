import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { BenefitEstimatorService } from './services/benefit-estimator.service';
import { BenefitEstimateResponseModel } from './models/benefit-estimate-response';
import { BenefitEstimateRequestModel } from './models/benefit-estimate-request';

@Component({
  selector: 'benefit-estimator',
  templateUrl: './benefit-estimator.component.html',
  styleUrls: ['./benefit-estimator.component.css']
})
export class BenefitEstimatorComponent implements OnInit {
  form: FormGroup;
  benefitEstimateResponse: BenefitEstimateResponseModel;

  constructor(private fb: FormBuilder, private benefitEstimatorService: BenefitEstimatorService) { }

  get dependentList() {
    return this.form.get('dependents') as FormArray;
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      employee: this.fb.group({
        firstName: this.fb.control('', [Validators.required]),
        lastName: this.fb.control('', [Validators.required])
      }),
      dependents: this.fb.array([])
    });
  }

  getEstimate() {
    if (!this.form.valid) { return; }
    const data = this.form.value as BenefitEstimateRequestModel;
    this.benefitEstimatorService.postBenefitEstimate(data).subscribe(x => {
      this.benefitEstimateResponse = x as BenefitEstimateResponseModel;
    });
  }

  addDependent() {
    this.dependentList.push(this.createDependent());
  }

  removeDependent(i: any) {
    this.dependentList.removeAt(i);
  }

  private createDependent(): FormGroup {
    return this.fb.group({
      firstName: this.fb.control(''),
      lastName: this.fb.control('')
    });
  }
}
