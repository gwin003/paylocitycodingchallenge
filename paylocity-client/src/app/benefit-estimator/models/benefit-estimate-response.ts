export class BenefitEstimateResponseModel {
    perPaycheckCost: number;
    yearlyCost: number;
}
