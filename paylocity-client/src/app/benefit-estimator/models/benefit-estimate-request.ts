export class BenefitEstimateRequestModel {
    employee: Person;
    dependents: Person[];
}

export class Person {
    firstName: string;
    lastName: string;
}
