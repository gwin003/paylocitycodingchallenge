import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitEstimatorComponent } from './benefit-estimator.component';
import { ReactiveFormsModule, FormsModule, FormArray } from '@angular/forms';
import { BenefitEstimateRequestModel } from './models/benefit-estimate-request';
import { Observable, of } from 'rxjs';
import { BenefitEstimateResponseModel } from './models/benefit-estimate-response';
import { BenefitEstimatorService } from './services/benefit-estimator.service';

describe('BenefitEstimatorComponent', () => {
  let component: BenefitEstimatorComponent;
  let fixture: ComponentFixture<BenefitEstimatorComponent>;
  let _mockBenefitEstimatorService: MockBenefitEstimatorService;

  class MockBenefitEstimatorService {
    postBenefitEstimate(request: BenefitEstimateRequestModel): Observable<BenefitEstimateResponseModel> {
      return of(new BenefitEstimateResponseModel());
    }
  }

  beforeEach(async(() => {
    _mockBenefitEstimatorService = new MockBenefitEstimatorService();
    TestBed.configureTestingModule({
      providers: [
        { provide: BenefitEstimatorService, useValue: _mockBenefitEstimatorService }
      ],
      declarations: [ BenefitEstimatorComponent ],
      imports: [FormsModule, ReactiveFormsModule],
    })
    .overrideComponent(BenefitEstimatorComponent, {
      set: {
        template: ''
      }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitEstimatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a getEstimate method', () => {
    expect(component.getEstimate).toBeTruthy();
  });

  it('should init a form with controls when its created', () => {
    expect(component.form).toBeDefined();
    expect(component.form.get('employee')).toBeDefined();
    expect(component.form.get('dependents')).toBeDefined();
    const dependents = component.form.get('dependents') as FormArray;
    expect(dependents.length).toBe(0);
    expect(component.form.valid).toBeFalsy();
  });

  it('should not call service when form is invalid', () => {
    spyOn(_mockBenefitEstimatorService, 'postBenefitEstimate').and.callThrough();
    component.getEstimate();
    expect(_mockBenefitEstimatorService.postBenefitEstimate).not.toHaveBeenCalled();
  });

  it('should be a valid form when first and last name is set', () => {
    expect(component.form.valid).toBeFalsy();

    component.form.get('employee').get('firstName').setValue('test');
    component.form.get('employee').get('lastName').setValue('test');

    expect(component.form.valid).toBeTruthy();
  });

  it('should call service when the form is valid', () => {
    spyOn(_mockBenefitEstimatorService, 'postBenefitEstimate').and.callThrough();

    component.form.get('employee').get('firstName').setValue('test');
    component.form.get('employee').get('lastName').setValue('test');
    expect(component.form.valid).toBeTruthy();

    component.getEstimate();

    expect(_mockBenefitEstimatorService.postBenefitEstimate).toHaveBeenCalled();
  });
});
