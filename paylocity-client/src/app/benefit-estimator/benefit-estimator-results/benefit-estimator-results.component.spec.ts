import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitEstimatorResultsComponent } from './benefit-estimator-results.component';

describe('BenefitEstimatorResultsComponent', () => {
  let component: BenefitEstimatorResultsComponent;
  let fixture: ComponentFixture<BenefitEstimatorResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitEstimatorResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitEstimatorResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
