import { Component, OnInit, Input } from '@angular/core';
import { BenefitEstimateResponseModel } from '../models/benefit-estimate-response';

@Component({
  selector: 'benefit-estimator-results',
  templateUrl: './benefit-estimator-results.component.html',
  styleUrls: ['./benefit-estimator-results.component.css']
})
export class BenefitEstimatorResultsComponent implements OnInit {
  @Input()
  estimateResult: BenefitEstimateResponseModel;

  ngOnInit() {

  }

}
