import { Component, EventEmitter, Output, Input } from '@angular/core';
import { BenefitEstimateResponseModel } from '../models/benefit-estimate-response';
import { FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'benefit-estimator-request',
  templateUrl: './benefit-estimator-request.component.html',
  styleUrls: ['./benefit-estimator-request.component.css']
})
export class BenefitEstimatorRequestComponent {
  @Output() postBenefitEstimate = new EventEmitter();
  @Output() addDependent = new EventEmitter();
  @Output() removeDependent = new EventEmitter();
  @Input() form: FormGroup;

  get dependentList() {
    return this.form.get('dependents') as FormArray;
  }

  onAddDependent() {
    this.addDependent.emit();
  }

  onRemoveDependent(i: any) {
    this.removeDependent.emit(i);
  }

  retrieveEstimate() {
    this.postBenefitEstimate.emit(this.form.value as BenefitEstimateResponseModel);
  }
}
