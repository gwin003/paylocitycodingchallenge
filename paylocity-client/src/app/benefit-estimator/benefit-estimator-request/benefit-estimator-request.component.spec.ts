import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitEstimatorRequestComponent } from './benefit-estimator-request.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BenefitEstimateRequestModel } from '../models/benefit-estimate-request';
import { BenefitEstimateResponseModel } from '../models/benefit-estimate-response';
import { Observable, of } from 'rxjs';
import { BenefitEstimatorService } from '../services/benefit-estimator.service';

describe('BenefitEstimatorRequestComponent', () => {
  let component: BenefitEstimatorRequestComponent;
  let fixture: ComponentFixture<BenefitEstimatorRequestComponent>;

    beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitEstimatorRequestComponent ],
      imports: [FormsModule, ReactiveFormsModule]
    })
    .overrideComponent(BenefitEstimatorRequestComponent, {
      set: {
        template: ''
      }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitEstimatorRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
