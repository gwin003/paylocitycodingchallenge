export const environment = {
  production: true,
  urls: {
    benefitEstimator: {
      postBenefitEstimate: 'http://100.26.16.203:5155/api/benefits'
    }
  }
};
