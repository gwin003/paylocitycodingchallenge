## PaylocityCodingChallenge
Repo for Paylocity Interview

## Demo
http://100.26.16.203/

## Overview
Being the technologies I am most familar with, I chose to construct a solution using a .NET Core WebApi backend with an Angular 7 front-end. I also mocked out a DB schema, but didn't implement the database into the application.

### **PaylocityChallenge.Api**

This project contains the API endpoint, BenefitController.

### **PaylocityChallenge.Contracts**

This project contains the contracts that the API will return to the calling application.

### **PaylocityChallenge.Core**

The Core project contains the business logic and calculations for the application. This is where most of the code for this solution resides, and I implemented tests for each piece.

### **PaylocityChallenge.Dao**

The Dao (data access layer) is responsible for interacting with the database. The only repositories in this project have mocked calls to the DB. Ideally, they'd actually query a database.

### **PaylocityChallenge.Test**

Tests for the Core and Dao projects.

### CreateSchema.sql
Not having a dedicated DBA at my current employer, I have become to go-to person for database related questions. I didn't see a real strong business need for a database based on the requirements of the challenge, but I decided to add this script to show how I would structure the tables.